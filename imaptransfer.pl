#!/usr/bin/perl 
=for comment
We had to move email out of one of our old proprietory email systems into a new systems via imap.
After changing the users password in the old system, we would run this script to grab their email and sync it to the new system.

We didn't have direct access to the user data or files on the old system, so this was a very hacky solution that I came up with,
but it did the job well enough for us.

=cut 

# Setup variables
my $user;
my $password;
my $host1="xx.xx.xx.xx";
my $host2="127.0.0.1";


print "Enter the email address you want to transfer: ";
chomp($user = <STDIN>);

print "Enter the users password: ";
chomp($password = <STDIN>);


system ("imapsync --host1=$host1 --host2=host2 --user1=$user --user2=$user --password1=$password --password2=$password --noauthmd5 --sep1=/ --prefix1=/");
