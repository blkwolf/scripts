#!/usr/bin/perl -w
#
use strict;
use DNS::ZoneParse;

=for comment

I once was tasked with moving dns information off of an old Linux based black box type webhosting device, that stored each zone
in an individual file, and convert each website to a new domain name in the process.

This script was designed to reach each website dns file, convert the domain name and output the information into a standard zone file
that could be read by Bind, which we would then move to the new dns server after completion.

=cut

my $filename;
my $origin;
my $zfile;

# File Extension (.com .net .org .ms .biz)
# my $fe = "ms";

# Open the local directory and read the files that match the extension listed in $fe
# and populate the filenames into the @Zones array
my @Zones = do {
    opendir my $dh, '.' or die "can't access directory\n";
    grep /\.ms$|.biz$|.com$|.org$|.net$/, readdir $dh;
};

 
foreach my $zf(@Zones) {
    # The next line is just for verification of the variable
    # print "$zf \n";
    $zfile = $zf;  # For some reason subroutines reset global variables so we make a copy of it and use that.
    DNSup($zf);
}



sub DNSup {
    my $zonefile = DNS::ZoneParse->new("$zfile", $origin);
    
    # Get a reference to the NameServer records
    my $ns = $zonefile->ns;
    
    # Change the DNS servers to match firefly's
    $ns->[0] = { host => 'ns1.dnsservername.net'};
    $ns->[1] = { host => 'ns2.dnsservername.net'};
    
    # update the serial number
    $zonefile->new_serial();
    
    # write the new zone file to disk 
    open NEWZONE, ">$zfile" or die "error";
    print NEWZONE $zonefile->output();
    close NEWZONE;
}
