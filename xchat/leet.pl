#!/usr/bin/perl -w 
# BlkW0lph'z L33t zCR1pt 4nd n0w Xch4t Plug1n :)

IRC::register("blkwolfs leet script","0.1","","");
IRC::print("Loading blkwolf\'s leet script\n");
IRC::add_command_handler("leet", "leet_start");

sub leet_start
{
    $phrase = shift(@_);
    check_phrase();
    IRC::command( "$phrase");
    return 1;
    
sub check_phrase
{

    while (index($phrase, "for")!= -1) {
        replace($phrase, "for", "4");
    }

    while (index($phrase, "you")!= -1) {
        replace($phrase, "you", "U");
    }
    while (index($phrase, "ate")!= -1) {
        replace($phrase, "ate", "8");
    }    
    
    while (index($phrase, "ck")!= -1) {
        replace($phrase, "ck", "k");
    }

    while (index($phrase, "a")!= -1) {
        replace($phrase, "a", "4");
    }

    while (index($phrase, "e")!= -1) {
        replace($phrase, "e", "3");
    }

    while (index($phrase, "f")!= -1) {
        replace($phrase, "f", "ph");
    }

    while (index($phrase, "i")!= -1) {
        replace($phrase, "i", "1");
    }

    while (index($phrase, "o")!= -1) {
        replace($phrase, "o", "0");
    }

    while (index($phrase, "s")!= -1) {
        replace($phrase, "s", "z");
    }
}

# Subroutine to replace the text
sub replace
  {
    ($phrase, $to_replace, $replace_with) = @_;

    substr ($phrase, index($phrase, $to_replace),
        length($to_replace), $replace_with);

    return $phrase;
  }

# Subroutine to randomly upper case the characters
#sub rand_case 
#{
#    while ($phrase =~/(.)/gs) {
#    $letter = $1;
#    $case = int( rand(100));
#    if ($case <=36) {
#    $letter = "\u$letter";
#    }
#    IRC::command( "\"$letter\"");
#   }
# }

}


