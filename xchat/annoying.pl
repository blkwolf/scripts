#!/usr/bin/perl -w
# Blkwolf's annoying irc script

IRC::register("blkwolfs annoying irc script","0.1","","");
IRC::print("Loading blkwolf\'s annoying irc script\n");
IRC::add_command_handler("rev", "rev_start");
IRC::add_command_handler("leet", "leet_start");
IRC::add_command_handler("rleet", "rev_leet");
IRC::add_command_handler("dot", "dot_matrix");
# Start reverse subroutine
sub rev_start
{
$phrase = shift(@_);
    chomp ($phrase);
    $revphrase = reverse($phrase);
IRC::command( "$revphrase");
return 1;
}


# Start leet talk subroutines
sub leet_start
{
    $phrase = shift(@_);
    check_phrase();
    $phrase = RandCase($phrase);
    IRC::command( "$phrase");
    return 1;
}

# Mix leet and reverse subroutines hehe
sub rev_leet
{
$phrase = shift(@_);
    check_phrase();
    $revphrase = reverse($phrase);
    IRC::command( "$revphrase");
    return 1;
}

# The Dot Matrix routine
# this evil one randomly inserts a . inside anything you type
sub dot_matrix
{
    $phrase = shift(@_);
    $phrase = RandDot($phrase);
    IRC::command( "$phrase");
    return 1;
}


sub check_phrase
{

    while (index($phrase, "for")!= -1) {
        replace($phrase, "for", "4");
    }

    while (index($phrase, "you")!= -1) {
        replace($phrase, "you", "U");
    }
    while (index($phrase, "ate")!= -1) {
        replace($phrase, "ate", "8");
    }    
    
    while (index($phrase, "ck")!= -1) {
        replace($phrase, "ck", "k");
    }

    while (index($phrase, "a")!= -1) {
        replace($phrase, "a", "4");
    }

    while (index($phrase, "e")!= -1) {
        replace($phrase, "e", "3");
    }

    while (index($phrase, "f")!= -1) {
        replace($phrase, "f", "ph");
    }

    while (index($phrase, "i")!= -1) {
        replace($phrase, "i", "1");
    }

    while (index($phrase, "o")!= -1) {
        replace($phrase, "o", "0");
    }

    while (index($phrase, "s")!= -1) {
        replace($phrase, "s", "z");
    }
}

# Subroutine to replace the text
sub replace
  {
    ($phrase, $to_replace, $replace_with) = @_;

    substr ($phrase, index($phrase, $to_replace),
        length($to_replace), $replace_with);

    return $phrase;
  }

# Randomize upper case
sub RandCase {
    my $text = shift(@_);
    @characters = split(//, $text);
    
    foreach my $char (@characters) {
        $up = int( rand(100));
        if ($up <=38) {
            $char = uc($char);
	} else {
	    $char = lc($char);
        }
    }
    return join("", @characters);
}

# Randomize the Dots....
sub RandDot {
    my $text = shift(@_);
    @characters = split(//, $text);
    
    foreach my $char (@characters) {
         if($char eq " ") {
	    next;
	}
	$dot = int( rand(100));
	if ($dot <=23) {
            $char = ".";
	} else {
	    $char = lc($char);
        }
    }
    return join("", @characters);
}
