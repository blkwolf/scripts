#!/usr/bin/perl -w

$xmmsquery = "/usr/local/bin/xmms-query";

IRC::register("XMMS-Query", "0.1", "", "");
IRC::add_command_handler( "xmms", "display_xmms" );

sub display_xmms 
{
    open( HANDLE, $xmmsquery."|" );
    if( $title = <HANDLE> ) {
        IRC::command( "/me is playing: \"$title\"" );
    }
    else {
        IRC::print( "XMMS ain't running!" );
    }
    close( HANDLE );
    return 1;
}
