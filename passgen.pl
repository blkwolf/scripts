#!/usr/bin/perl

=for comment

Very simple random login password generator
reads list of lower case words between 3 & 5 letters
randomly ties two words together with a string character
also randomly inserting spaces and/or uppercasing first
letter of words.
ver.01 09/13/1999

=cut

@character = qw(! 1 @ 2 # 3 $ 4 % 5 & 6 * 7 = 8 - 9 + 0);

# Get home directory
$home = $ENV{HOME};

open(DICT, "$home/bin/wordlist") ||
die "Can't open dict: $!";

rand_word();

sub rand_word {
    $wlist[0] = <DICT>;
    while ($next = <DICT>) {
        push (@wlist, $next);
    }

    $length = @wlist;
    rand_number();
    chomp ($list = $wlist[$random]);
    rand_case();
    $wfirst = $list;

    rand_number();
    chomp ($list =  $wlist[$random]);
    rand_case();
    $wsec = $list;

    $length = @character;
    rand_number();
    $char = $character[$random];


    $lfirst = length($wfirst);
    $lsec = length($wsec);
    $total = ($lfirst + $lsec);
    if ($total >= 12) {
        rand_word();
    } 

    if ($total eq 6) {
        # space before character?
        $space = int( rand(100));
        if ($space <= 50) {
            $char = " $char";
        }   
        $space = int( rand(100));
        if ($space <= 50) {
            $char = "$char ";
        }
    }
}
    
    sub rand_number {
    $random = int( rand($length));
    }
    
sub rand_case {
    # randomize upper case of first letter
    $case = int( rand(100));
    if ($case <=30) {
        $list = "\u$list";
    }
}

system ("clear");
print "BlkWolf's Perl Password Generator\n";
print "Your new password is: \n"; 
print "$wfirst","$char","$wsec \n";

close(DICT);



